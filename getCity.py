import requests
from lxml import etree
import time
import random
from fake_useragent import UserAgent
import re

class LianjiaSpider(object):
    def __init__(self):
        self.url = 'https://www.lianjia.com/city/'
        self.regionList = []

    def parse_html(self,url):
        headers = {'User-Agent':UserAgent().random}
        html = requests.get(url=url,headers=headers).content.decode('utf-8','ignore')
        self.get_data(html)

    def get_data(self,html):
        p = etree.HTML(html)
        # 基准xpath: [<element li at xxx>,<element li>]
        li_list = p.xpath('//ul[@class="city_list_ul"]/li[@class="city_list_li city_list_li_selected"]')
        # for遍历,依次提取每个房源信息,放到字典item中
        item = {}
        for li in li_list:
            province = li.xpath('.//div[@class="city_list_tit c_b"]/text()')[0]
            citys= li.xpath('.//a/text()')
            cityLinks = li.xpath('.//a/@href')
            cityNames = []
            for cityLink in cityLinks:
                cityNames.append(re.findall(r"://(.*?)\.",cityLink)[0])
            print(province)
            print(citys)
            print(cityNames)
            for cityIndex,cityName in enumerate(cityNames):
                # https://bj.lianjia.com/

                try:  # 有的city只有loupan，没有二手房
                    url = "https://{}.lianjia.com/ershoufang/".format(cityName)
                    content = requests.get(url, headers={'User-Agent': UserAgent().random}).content.decode('utf-8','ignore')
                except:
                    continue

                regions = etree.HTML(content).xpath('//div[@data-role="ershoufang"]//a/text()')
                print(regions)
                regionLinks = etree.HTML(content).xpath('//div[@data-role="ershoufang"]//a/@href')
                regionLinks = [url[:-1].replace("/ershoufang","") + x for x in regionLinks]

                # print(regionLinks)
                # exit()
                for i,e in enumerate(regionLinks):
                    indices = [x.start() for x in re.finditer("/",e)]
                    region = e[indices[-2]+1:indices[-1]]

                    # content = requests.get(url=url, headers={'User-Agent': UserAgent().random}).content.decode('utf-8', 'ignore')
                    # num = int(etree.HTML(content).xpath("//h2[@class='total fl']/span/text()")[0].strip())
                    # if num<0:
                    #     continue

                    self.regionList.append((province, citys[cityIndex], cityName, regions[i], region))
                    print((province, citys[cityIndex], cityName, regions[i], region))
                # print(url)
            print("*"*100)
        print(self.regionList)

    def run(self):
        self.parse_html(self.url)

if __name__ == '__main__':
    spider = LianjiaSpider()
    spider.run()
