import math
import random
import re
import time
import pandas
import requests
from lxml import etree


city_url = r'https://sz.lianjia.com/ershoufang/luohuqu/'
down = 42
up = 7200

inter_list = [(int(42), int(7200))]

def binary(inter):
    lower = inter[0]
    upper = inter[1]
    ave = int((upper - lower) / 2)
    inter_list.remove(inter)
    print("已经缩小价格区间：", inter)
    inter_list.append((lower, lower + ave))
    inter_list.append((lower + ave, upper))
    print(inter_list)


pagenum = {}
headers = {
    'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36"
}


def get_num(inter):  # 获取两个价格之间的房子数据
    link = city_url + f'bp{inter[0]}ep{inter[1]}/'
    # print(link)
    r = requests.get(link, headers=headers)
    r = r.content.decode('utf-8','ignore')
    num = int(etree.HTML(r).xpath("//h2[@class='total fl']/span/text()")[0].strip())
    pagenum[(inter[0], inter[1])] = num
    # print("".format(num))
    return num

print(inter_list[0])
totalnum = get_num(inter_list[0])

judge = True
while judge:
    a = [get_num(x) > 3000 for x in inter_list]
    if True in a:
        judge = True
        for i in inter_list:
            if get_num(i) > 3000:
                binary(i)
    else:
        judge = False
print("价格区间缩小完毕！", inter_list)
print(f"一共有{totalnum}条房源信息")
#  print(pagenum)
exit()
